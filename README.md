# RiverSide


Ce projet a été réalisé dans le cadre du cours de WebGl en licence 3 du cursus informatique de l’université de Limoges. Le but était de réaliser un cours d’eau réaliste animé et d’implémenter des effets d’imagerie 3D a la discrétion des groupes.


Le projet a été réalisé en binôme. Ma contribution fut :
- La modélisation de l’environnement.
- La création des textures
- Les effet et l’animation du cours d’eau
- L’effet du ciel en fonction d’un slider représentant l’heure de la journée.

Nous avons utilisé les technologie Javascript pour la logique et WebGl (à travers three.js) pour l’affichage dans le navigateur.

Les effets du cours d’eau sont en grande partie issue des travaux de Jonas WAGNER (http://29a.ch/slides/2012/webglwater/) ainsi que de l’implémentation de ‘mrdoob’, créateur de three.js.
