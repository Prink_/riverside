/* ----------------------- GLOBAL VARIABLES --------------------------------- */
var WIDTH = window.innerWidth, HEIGHT = window.innerHeight,
container = document.getElementById('container'),
camera, controls, scene, renderer, loader;
//Scene elements
var TerrainTexture, terrainPMat;
var mountainTexture, grotte, montagne;
var waterPond, waterUniforms, waterAttribute, waterSMat, waterGeo, riverCamera;
var waterShader, waterRenderer, dLight, waterNormalMap, riverRTexture;
var skybox, skySMat, nightBox;
var lapin;
var gazebo, gazeboText, gazeboMat;
var slimeGeo, slimeMat, slimeBox;

var time = 14;
function init(){
    scene = new THREE.Scene();
    
    camera = new THREE.PerspectiveCamera(45, WIDTH/HEIGHT, 0.1,100000);
    camera.position.set(100,100,100);
    camera.up.set(0,1,0);
    camera.lookAt(scene.position);
    
    renderer = new THREE.WebGLRenderer();
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.setClearColor(0x050510,1);
    renderer.shadowMapEnabled = true;
    renderer.clear();
    
    container = document.getElementById('container');
    container.appendChild(renderer.domElement);
    
    this.clock = new THREE.Clock();
    
    controls = new THREE.OrbitControls(camera, renderer.domElement);
    
    loader = new THREE.JSONLoader();
};

function add_Scene_Content(){
    dLight = new THREE.DirectionalLight(0xc0bfad, 2.5);
    dLight.position.set(1,1,1);
    dLight.castShadow = true;
    scene.add(dLight);
    
/* ---------------------------- TERRAIN ------------------------------------- */
var grass = new THREE.ImageUtils.loadTexture('image/Grass/GrassGreenTexture0006.jpg');
grass.format = THREE.RGBFormat;
grass.wrapS = THREE.ReapeatWrapping;
grass.wrapT = THREE.ReapeatWrapping;
grass.repeat.set(120,120);

var grassMat = new THREE.MeshPhongMaterial({
    color:0xCCCCCC, side: THREE.DoubleSide, map: grass,
    ambiant: 0x000000, shininess: 20, specular: 0x000000
});

TerrainTexture =  new THREE.ImageUtils.loadTexture('image/textureTerrain.png');
TerrainTexture.format = THREE.RGBFormat;
terrainPMat = new THREE.MeshPhongMaterial({
    map: TerrainTexture,color: 0xCCCCCC,
    ambiant: 0x000000, shininess: 20, specular: 0x000000
});

loader.load("Model/terrain.js",function(geometry){
    geometry.computeVertexNormals();
    terrain = new THREE.Mesh(geometry,terrainPMat);
    
    terrain.scale.set(1,1,1);
    terrain.position.set(-440,-280,0);
    terrain.castShadow = true;
    terrain.receiveShadow = true;
    scene.add(terrain);
});

loader.load("Model/terrainSuite.js",function(geometry){
    geometry.computeVertexNormals();
    terrainSuite = new THREE.Mesh(geometry,grassMat);
    
    terrainSuite.scale.set(11.75,11.75,11.75);
    terrainSuite.position.set(-440,-31,-12918);
    terrainSuite.castShadow = true;
    terrainSuite.receiveShadow = true;
    scene.add(terrainSuite);
});
/* ------------------------- MOUNTAIN ------------------------------------- */
    mountainTexture = new THREE.ImageUtils.loadTexture('image/TextureMontagne2.png');

    loader.load("Model/montagne.js",function(geometry){
        geometry.computeVertexNormals();
        montagne = new THREE.Mesh(geometry,new THREE.MeshPhongMaterial({
            map: mountainTexture,
            color: 0x505050,
            shininess: 0
        }));

        montagne.scale.set(1,1,1);
        montagne.position.set(-500,-100,-1000);
        montagne.castShadow = true;
        montagne.receiveShadow = true;
        scene.add(montagne);
    });

    grotte = new THREE. Mesh(new THREE.PlaneBufferGeometry(500,500), 
                             new THREE.MeshBasicMaterial({color: 0x000000}));
    grotte.position.set(0,100,-1000);
    scene.add(grotte);
/* ----------------------------- WATER -------------------------------------- */
    var RiverTopTexture =  new THREE.ImageUtils.loadTexture('image/reflection.jpg');
    RiverTopTexture.wrapS = THREE.ReapeatWrapping;
    RiverTopTexture.wrapT = THREE.ReapeatWrapping;
    RiverTopTexture.repeat.set(5,5);
    
    waterNormalMap = new THREE.ImageUtils.loadTexture("image/riverNormals.jpg");
    waterNormalMap.wrapS = waterNormalMap.wrapT = THREE.RepeatWrapping; 
    waterUniforms = THREE.UniformsUtils.merge([
        THREE.UniformsLib['lights'],{
        Color:{type:'c', value: new THREE.Color(0.3,0.5,0.6)},
        normalSampler:{type: 't', value: []},
        time:{type: 'f', value: 0},
        waterTexture:{type:'t',value:[]},
        reflectSampler:{type:'t',value:[]},
        textureMatrix:{type: 'm4', value: new THREE.Matrix4()}        
    }]);
    waterUniforms.normalSampler.value = waterNormalMap;
    waterUniforms.waterTexture.value = RiverTopTexture;
    waterSMat = new THREE.ShaderMaterial({
        uniforms: waterUniforms,
        vertexShader: document.getElementById('waterVertex').textContent,
        fragmentShader: document.getElementById('waterFragment').textContent,
        transparent: true,
        lights: true,
        side: THREE.DoubleSide
    });

    waterGeo = new THREE.PlaneBufferGeometry(700,2400);
    waterPond = new THREE.Mesh(waterGeo,waterSMat);
    waterPond.receiveShadow = true;
    waterPond.position.set(40,-63,0);
    waterPond.rotation.x=-Math.PI/2;
    scene.add(waterPond);
    
/*    REFLECTION     */
    riverCamera = camera.clone();
    riverRTexture = new THREE.WebGLRenderTarget(
        window.innerWidth,window.innerHeight,{format: THREE.RGBFormat}
    );
    riverRTexture.generateMipmaps = false;
    waterUniforms.reflectSampler.value = riverRTexture;
    
/* ---------------------------- SKY BOX ------------------------------------- */
    var sbCube = THREE.ImageUtils.loadTexture('image/sky_box.png');

    var skyUniforms ={
        SkyColor:{type:'c',value: new THREE.Color(0x00BFFF)},
        SkySampler:{type:'t',value:sbCube},
        time:{type:'f',value:0},
        sunPosition:{type:'v3',value:new THREE.Vector3()},
        bodyPow:{type: 'f', value: 0.6},
        haloPow:{type: 'f', value: -5.0},
        balance:{type: 'f', value: 0.0}
    };

    skySMat = new THREE.ShaderMaterial({
        uniforms: skyUniforms,
        vertexShader: document.getElementById('skyVertex').textContent,
        fragmentShader: document.getElementById('skyFragment').textContent,
        side: THREE.BackSide, transparent: true
    });

    var skyGeo = new THREE.BoxGeometry(25000,25000,25000);
    var skyBox = new THREE.Mesh(skyGeo, skySMat);
    scene.add(skyBox);
/* ------------------------------- NIGHT BOX -------------------------------- */
    var stars = THREE.ImageUtils.loadTexture('image/night_skybox.png');
    
    var nightShader = new THREE.ShaderMaterial({
        uniforms:{starSampler:{type:'t', value: stars}},
        vertexShader: document.getElementById('nightVertex').textContent,
        fragmentShader: document.getElementById('nightFragment').textContent,
        side: THREE.BackSide, transparent: true
    });
    
    nightBox = new THREE.Mesh(
            new THREE.BoxGeometry(50000,50000,50000),nightShader
        );
    scene.add(nightBox);
/* -------------------------------- LAPIN ----------------------------------- */
    var textureLapin = new THREE.ImageUtils.loadTexture('image/Bunny.png');
    var LapinMat = new THREE.MeshPhongMaterial({
        color:0xCCAAAA, shininess: 20, 
        specular: 0x040404, map: textureLapin
    });
    loader.load( "model/Bunny.js",function(geometry){
        geometry.computeVertexNormals();
        lapin = new THREE.Mesh(geometry,LapinMat);
        
        lapin.scale.set(100,100,100);
        
        lapin.position.set(0,-33,0);
        lapin.receiveShadow = true;
        lapin.castShadow = true;
        scene.add(lapin);
    });
/* ---------------------- PONT ------------------------------- */
    var pont, pontText;
    pontText = THREE.ImageUtils.loadTexture('image/textPont.png');
    var pontMat = new THREE.MeshPhongMaterial({color:0xffffff, shininess: 30, specular: 0x000000});
    pontMat.map = pontText;

    loader.load("Model/pont.js",function(geometry){
        geometry.computeVertexNormals();
        pont = new THREE.Mesh(geometry,pontMat);

        pont.scale.set(0.1,0.1,0.1);
        pont.position.set(5,-55,0);
        pont.castShadow = true;
        pont.receiveShadow = true;
        scene.add(pont);
    });
/* -------------------------- GAZEBO ---------------------------------------- */

    gazeboText = THREE.ImageUtils.loadTexture('image/textureGazebo.png');
    gazeboMat = new THREE.MeshPhongMaterial({
        color:0xffffff, shininess: 30, side: THREE.DoubleSide,
        specular: 0x000000, map: gazeboText
    });

    loader.load("Model/gazebo.js",function(geometry){
        geometry.computeVertexNormals();
        gazebo = new THREE.Mesh(geometry,gazeboMat);

        gazebo.scale.set(0.2,0.2,0.2);
        gazebo.position.set(-1000,-20,0);
        gazebo.rotation.y -= Math.PI/2.0;
        gazebo.castShadow = true;
        gazebo.receiveShadow = true;
        scene.add(gazebo);
    });
/* ------------------------------ SLIME ------------------------------------- */
    slimeGeo = new THREE.BoxGeometry(100, 100, 100);
    slimeMat = new THREE.ShaderMaterial({
        uniforms: THREE.UniformsUtils.merge([
        THREE.UniformsLib['lights'],{ slimeAmbient: {type: 'c',
                                   value: new THREE.Color(0x208020)},
                    slimeDiffuse: {type: 'c',
                                   value: new THREE.Color(0x00FF80)},
                    slimeSpecular: {type: 'c',
                                    value: new THREE.Color(0xFF0000)},
                    slimeTransparency: {type: 'f',
                                        value: 0.6},
                    genBlobPosition: {type: 'v3v',
                                      value: [new THREE.Vector3(0,0,0),
                                              new THREE.Vector3(0,0,0),
                                              new THREE.Vector3(0,0,0)]},
                    genBlobRadius: {type: 'fv1', value: [1, 5, 10]},
                    blobRadius: {type: 'f', value: 2}}]),
        vertexShader: document.getElementById('slimeVertex').textContent,
        fragmentShader: document.getElementById('slimeFragment').textContent,
        side: THREE.DoubleSide,transparent: true, lights: true});

    slimeBox = new THREE.Mesh(slimeGeo, slimeMat);
    slimeBox.position.set(-1000,10,0);
    scene.add(slimeBox);
/*----------------------------------------------------------------------------*/
};
//Cr�� une image pour la reflection de l'eau en utilisation le cour d'eau comme
//"near plane" pour suprimer ce qui est en dessous de l'eau
function riverReflectionUpdate(){ 
    var mirrorPlane = new THREE.Plane();
    var normal = new THREE.Vector3( 0, 0, 1 );
    var mirrorWorldPosition = new THREE.Vector3();
    var cameraWorldPosition = new THREE.Vector3();
    var rotationMatrix = new THREE.Matrix4();
    var lookAtPosition = new THREE.Vector3(0, 0, -1);
    var clipPlane = new THREE.Vector4();
    
    waterPond.updateMatrixWorld();
    camera.updateMatrixWorld();
    
    mirrorWorldPosition.setFromMatrixPosition(waterPond.matrixWorld);
    cameraWorldPosition.setFromMatrixPosition(camera.matrixWorld);
    
    rotationMatrix.extractRotation(waterPond.matrixWorld);
    
    normal.set(0,0,1);
    normal.applyMatrix4(rotationMatrix);
    
    var view = mirrorWorldPosition.clone().sub(cameraWorldPosition);
    view.reflect(normal).negate();
    view.add(mirrorWorldPosition);
    
    rotationMatrix.extractRotation(camera.matrixWorld);
    
    lookAtPosition.set(0,0,-1);
    lookAtPosition.applyMatrix4(rotationMatrix);
    lookAtPosition.add(cameraWorldPosition);
    
    var target = mirrorWorldPosition.clone().sub(lookAtPosition);
    target.reflect(normal).negate();
    target.add(mirrorWorldPosition);
    
    var MirrorUp = new THREE.Vector3(0,-1,0);
    MirrorUp.applyMatrix4(rotationMatrix);
    MirrorUp.reflect(normal).negate();
    
    riverCamera.position.copy(view);
    riverCamera.up = MirrorUp;
    riverCamera.lookAt(target);
    
    riverCamera.updateProjectionMatrix();
    riverCamera.updateMatrixWorld();
    riverCamera.matrixWorldInverse.getInverse(riverCamera.matrixWorld);
    
    var textureMatrix = new THREE.Matrix4();

    textureMatrix.set(0.5,0.0,0.0,0.5,
                      0.0,0.5,0.0,0.5,
                      0.0,0.0,0.5,0.5,
                      0.0,0.0,0.0,1.0);
    textureMatrix.multiply(riverCamera.projectionMatrix);
    textureMatrix.multiply(riverCamera.matrixWorldInverse);
    
    mirrorPlane.setFromNormalAndCoplanarPoint(normal,mirrorWorldPosition);
    mirrorPlane.applyMatrix4(riverCamera.matrixWorldInverse);
    
    clipPlane.set(mirrorPlane.normal.x,mirrorPlane.normal.y,mirrorPlane.normal.z,mirrorPlane.constant);
    
    var q = new THREE.Vector4();
    var projectionMatrix = riverCamera.projectionMatrix;
    
    q.x = (Math.sign(clipPlane.x) + projectionMatrix.elements[8]) / projectionMatrix.elements[0];
    q.y = (Math.sign(clipPlane.y) + projectionMatrix.elements[9]) / projectionMatrix.elements[5];
    q.z = -1.0;
    q.w = ( 1.0 + projectionMatrix.elements[10]) / projectionMatrix.elements[14];
    
    var c = new THREE.Vector4();
    c = clipPlane.multiplyScalar(2.0/clipPlane.dot(q));
    
    projectionMatrix.elements[2] = c.x;
    projectionMatrix.elements[6] = c.y;
    projectionMatrix.elements[10] = c.z + 1.0;
    projectionMatrix.elements[14] = c.w;
    
    waterUniforms.textureMatrix.value = textureMatrix;
}

function add_gui(){
/* ------------------------------- DAT.GUI ---------------------------------- */
    text = {hour: 14};
    gui = new dat.GUI();
    var folder1 = gui.addFolder('Time');
    var tHour = folder1.add(text, 'hour').min(0).max(24).step(0.1).listen().name('Heure');
    
    tHour.onChange(function(value){time = value;});
}
//D�fini la direction de la DirectionLight en fonction de l'heure donn�e
//ainsi que l'orientation de la skybox de nuit.
function DayNightCycle(time){
    dLight.position.set(-Math.sin(time*Math.PI/12),-Math.cos(time*Math.PI/12),0.5);
    nightBox.rotation.z = -Math.PI / 12 * time;
};

function render(){
    waterPond.visible = false;
    renderer.render(scene, riverCamera, riverRTexture, true);
    waterPond.visible = true;
    
    renderer.render(scene,camera);    
};

function animate(){
    var t = clock.getDelta();
    DayNightCycle(time);
    riverReflectionUpdate();
    //- ------- UNIFORM UPDATES -------
    waterSMat.uniforms.time.value += t;
    skySMat.uniforms.time.value += t;
    skySMat.uniforms.sunPosition.value = dLight.position;    
    dLight.intensity = dLight.position.y*2+1;
    
    /* SLIME
    Parametres a modifier en fonction du temps:
    - Uniforms: genBlobPosition, peut-etre les radius
    - slimeBox.position
    Parametres constraints par rapport aux autres:
    - genBlobPosition[k].add(slimeBox.position) doit toujours apparaitre
    - genBlobPosition 0+ radius ne doit pas depasser les dimensions de slimeBox, sinon ca coupe le slime*/
    var slimet = clock.getElapsedTime();
    var gbp = slimeMat.uniforms.genBlobPosition;
    gbp.value[0].set(15 * Math.cos(slimet), 0, 0);
    gbp.value[1].set(0, 4 * Math.sin(slimet), 0, 0);
    gbp.value[2].set(0, -7 * Math.cos(slimet), 5 * Math.sin(slimet));
    for (k = 0; k < 3; ++k){
        gbp.value[k].add(slimeBox.position);
    }
    
    
    requestAnimationFrame(animate);
    render();
    controls.update();
};

init();
add_gui();
add_Scene_Content();
animate();